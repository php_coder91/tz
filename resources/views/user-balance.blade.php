<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

</head>
<body>
    <div class="flex-center position-ref full-height">

        <div class="container">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Логин</th>
                            <th>Баланс</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>988799</td>
                            <td>{{$userBalance->balance}}</td>
                        </tr>
                    </tbody>
                </table>
        </div>
    </div>
</body>
</html>
