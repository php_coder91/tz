<?php

namespace App\Http\Controllers;

use App\Models\UserBalance;
use Illuminate\Http\Request;
use App\Services\SiteWalker;

class CrawlController extends Controller
{
    /**
     * Get user balance from https://spb.rutaxi.ru/car_owners.html
     */
    public function getUserBalance()
    {
        $siteWalker = new SiteWalker();
        $siteWalker->getUserBalance();

        return redirect()->route('show_user_balance');
    }

    public function showUserBalance()
    {
        $userBalance = UserBalance::query()->where(['login' => '988799'])->first();

        return view('user-balance', ['userBalance' => $userBalance]);
    }
}
