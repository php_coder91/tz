<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBalance extends Model
{
    protected $table = "user_balances";

    protected $fillable = ['login', 'balance'];
}
