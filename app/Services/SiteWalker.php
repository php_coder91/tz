<?php

namespace App\Services;

use App\Models\UserBalance;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;

class SiteWalker
{
    public $httpClient;

    public function __construct()
    {
        $this->httpClient = new Client();
    }

    /**
     * Make login post request.
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function login()
    {
        $cookieJar = new CookieJar ();
        $result = $this->httpClient->request('POST', 'https://spb.rutaxi.ru/car_owners.html', [
            'form_params' => ['action' => 'login', 'login' => '988799', 'password' => '136'],
            'cookies' => $cookieJar
        ]);

        return $result;
    }

    /**
     * Get user balance from response.
     */
    public function getUserBalance()
    {
       $result = $this->login();

        $balance = $this->parseHtmlText($result->getBody()->getContents());

        $this->saveBalanceToDatabase($balance);
    }

    /**
     * Get balance text from html text.
     *
     * @param $htmlText
     * @return bool|string
     */
    public function parseHtmlText($htmlText)
    {
        $balance = $this->getSubString($htmlText, '<dd>', '</dd>');
        $balance = mb_substr($balance, 6);

        return $balance;
    }

    /**
     * Gets substring between two strings.
     *
     * @param $string
     * @param $start
     * @param $end
     * @return bool|string
     */
    public function getSubString($string, $start, $end)
    {
        $string = ' ' . $string;
        $startPosition = strpos($string, $start);
        
        if ($startPosition == 0)
            return '';

        $startPosition += strlen($start);
        $len = strpos($string, $end, $startPosition) - $startPosition;
        
        return substr($string, $startPosition, $len);
    }

    /**
     * Save user balance to database.
     *
     * @param $balance
     */
    public function saveBalanceToDatabase($balance)
    {
        $userBalance = UserBalance::firstOrNew(['login' => '988799']);
        $userBalance->balance = $balance;
        $userBalance->save();
    }
}