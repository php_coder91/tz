<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Инструкция по установке проекта

- Скопировать или скачать проект с битбакета <b>git clone https://php_coder91@bitbucket.org/php_coder91/tz.git</b>
- Настроить виртуальный хост
- Выполнить команды <b> <br>composer install <br>php artisan migrate <br>php artisan key:generate </b>

## Страницы приложения
- <b>http://<domain-name>/testpage </b> - вывести баланс пользователя
- <b>http://<domain-name>/get-user-balance </b> - спарсить баланс с сайта https://spb.rutaxi.ru/car_owners.html